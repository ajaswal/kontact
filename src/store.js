import { createStore, compose, applyMiddleware } from "redux";
import reducers from "./reducers";
import ReduxLogger from "redux-logger";

// Middleware
let middleware = [];

const configureStore = preloadedState => {
  const enhancers = [];

  if (process.env.NODE_ENV === "dev") {
    middleware.push(ReduxLogger);
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__;

    if (typeof devToolsExtension === "function") {
      enhancers.push(devToolsExtension());
    }
  }

  const store = createStore(
    reducers,
    preloadedState,
    compose(
      applyMiddleware(...middleware),
      ...enhancers
    )
  );

  return store;
};

const store = configureStore();

export default store;
