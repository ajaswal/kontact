import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const Container = styled.div`
  font-family: sans-serif;
  margin: 0 10px 10px 0;
`;

const Label = styled.label`
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.75;
  letter-spacing: normal;
  color: #546271;
  text-transform: uppercase;
`;

const Input = styled.input`
  font-size: 12px;
  font-weight: normal;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.75;
  letter-spacing: normal;
  color: #546271;
  border: none;
  outline: none;
  border: 1px solid #dadada;
  border-radius: 3px;
  background-color: #ffffff;
  padding: 0 5px;

  &:focus {
    outline-offset: 0;
  }
`;

class TextField extends Component {
  render() {
    const { valid, message, ...inputProps } = this.props;
    const { id, label } = inputProps;

    return (
      <Container>
        <Label htmlFor={id}>{label}</Label>
        <div>
          <Input
            aria-describedby={`${id}-desc`}
            aria-invalid={valid ? "false" : "true"}
            {...inputProps}
          />
        </div>
        {!valid && message.length > 0 ? (
          <div id={`${id}-desc`}>{message}</div>
        ) : null}
      </Container>
    );
  }
}

TextField.propTypes = {
  valid: PropTypes.bool,
  message: PropTypes.string,
  id: PropTypes.string.isRequired,
  name: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  onFocus: PropTypes.func,
  onChange: PropTypes.func,
  onInput: PropTypes.func,
  onKeyUp: PropTypes.func,
  onKeyDown: PropTypes.func,
  onBlur: PropTypes.func,
  value: PropTypes.string
};

TextField.defaultProps = {
  id: "textfield",
  name: "",
  label: "",
  type: "text",
  valid: true,
  message: "Input is not valid",
  disabled: false,
  placeholder: ""
};

export default TextField;
