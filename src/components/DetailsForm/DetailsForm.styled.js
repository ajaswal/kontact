import styled from "styled-components";

const FormContainer = styled.div`
  padding: 20px;
`;
const RowLine = styled.div`
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  font-family: sans-serif;
`;
const ButtonContainer = styled.div`
  width: 50%;
  display: flex;
  order: 2;
`;
const Button = styled.button`
  width: 100%;
  height: 40px;
  border: 0;
  margin: 10px 10px 0 0px;
  background: ${props => props.backgroundColor || "gray"};
  border-radius: 3px;
  font-size: 16px;
  color: ${props => props.color || "white"};
`;
const FileUploadContainer = styled.div`
  position: relative;
  display: flex;
  width: 50%;
  order: 1;
`;
const UploadLabel = styled.span`
  padding: 10px;
  color: #fff;
  background: #627b8b;
  border-radius: 3px;
  font-size: 16px;
  width: 100%;
  margin: 10px 10px 0 0px;
  display: flex;
  align-items: center;
  justify-content: center;

  &:hover {
    cursor: pointer;
    background: #000;
  }
`;
const FileUploadInput = styled.input`
  position: absolute;
  top: 0;
  right: 0;
  margin: 0;
  padding: 0;
  font-size: 40px;
  cursor: pointer;
  opacity: 0;
  filter: alpha(opacity=0);
`;
const SectionHeading = styled.h6`
  color: #b0b8bc;
  text-transform: uppercase;
  font-family: sans-serif;
  border-bottom: 1px solid;
  font-weight: normal;
`;
const FormHeading = styled.h2`
  font-family: sans-serif;
  color: #2c3e50;
`;

export {
  FormContainer,
  FormHeading,
  RowLine,
  ButtonContainer,
  Button,
  FileUploadContainer,
  UploadLabel,
  FileUploadInput,
  SectionHeading
};
