import { CARD } from "./types";
import { createAction } from "./utils";

export const card = {
  saveCardDetails: data =>
    createAction(CARD.SAVE, {
      data,
      fetching: true,
      success: false,
      error: null
    }),
  success: data =>
    createAction(CARD.SUCCESS, {
      ...data,
      fetching: false,
      success: true,
      error: null
    }),
  failure: error =>
    createAction(CARD.FAILURE, {
      ...error,
      fetching: false,
      success: false
    })
};
