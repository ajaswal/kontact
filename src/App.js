import React, { Component } from "react";
import styled from "styled-components";
import DetailsForm from "./components/DetailsForm";
import CardPreview from "./components/CardPreview";

const AVATAR_URL =
  "https://upload.wikimedia.org/wikipedia/commons/7/7c/Profile_avatar_placeholder_large.png";

const Container = styled.div`
  padding: 0;
  margin: 0;
  list-style: none;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  height: 100vh;
  align-items: center;
  justify-content: center;
`;

export default class App extends Component {
  state = {
    avatar: AVATAR_URL
  };

  handleFormChange = event => {
    try {
      const name = event.target.name;
      let value = event.target.value;

      this.setState({
        [name]: value
      });
    } catch (error) {
      console.error("Change handler crashed", error);
    }
  };

  handleAvatarUpload = event => {
    try {
      const reader = new FileReader();
      const file = event.target.files[0];
      const self = this;
      
      reader.onload = function(upload) {
        self.setState({
          avatar: upload.target.result
        });
      };
      reader.readAsDataURL(file);
    } catch (error) {
      console.error("Uploading Avatar", error);
    }
  };

  render() {
    return (
      <Container>
        <DetailsForm
          handleFormChange={this.handleFormChange}
          handleAvatarUpload={this.handleAvatarUpload}
        />
        <CardPreview {...this.state} />
      </Container>
    );
  }
}
