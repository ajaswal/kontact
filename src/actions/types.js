import { createActionTypes } from "./utils";

export const CARD = createActionTypes("CARD", [
  "GET",
  "SAVE",
  "DELETE",
  "SUCCESS",
  "FAILURE",
  "NOTIFICATIONS"
]);
