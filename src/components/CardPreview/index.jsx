import React, { Component } from "react";
import {
  CardContainer,
  Card,
  List,
  ListItem,
  Header,
  ImageCover,
  AvatarImage,
  CardTitle,
  VCard,
  ItemLabel,
  ItemValue,
  DodgyAddressLine,
  WrapIntoSingle,
  ListItemLeft,
  ListItemRight,
  Link,
  PlaceholderHeading
} from "./CardPreview.styled";

export default class CardPreview extends Component {
  render() {
    const {
      givenName = "",
      surName = "",
      email,
      phone,
      houseName,
      street,
      suburb,
      state,
      postCode,
      country,
      avatar
    } = this.props;
    return (
      <div>
        <PlaceholderHeading>hCard Preview</PlaceholderHeading>
        <br/>
        <VCard className="vcard">
          <CardContainer>
            <Card>
              <Header>
                <CardTitle className="fn">
                  {givenName + " " + surName}
                </CardTitle>
                <ImageCover>
                  <AvatarImage className="photo" src={avatar} alt="avatar" />
                </ImageCover>
              </Header>
              <List>
                <ListItem>
                  <ItemLabel>Email</ItemLabel>
                  <ItemValue>
                    <Link className="email" href={email}>
                      {email}
                    </Link>
                  </ItemValue>
                </ListItem>

                <ListItem>
                  <ItemLabel>Phone</ItemLabel>
                  <ItemValue>
                    <span className="tel">{phone}</span>
                  </ItemValue>
                </ListItem>
                <ListItem>
                  <ItemLabel>Address</ItemLabel>
                  <ItemValue>
                    <span className="adr">
                      <span className="street-address">
                        {houseName}&nbsp;
                        {street}
                      </span>
                      <DodgyAddressLine>
                        <span className="locality">{suburb}</span>
                        {state ? ", " : null}
                        <span className="region" title={state}>
                          {state}
                        </span>
                      </DodgyAddressLine>
                    </span>
                  </ItemValue>
                </ListItem>

                <ListItem>
                  <ItemLabel />
                  <ItemValue>
                    <div />
                  </ItemValue>
                </ListItem>
                <WrapIntoSingle>
                  <ListItemLeft>
                    <ItemLabel>Postcode</ItemLabel>
                    <ItemValue paddingValue="10">
                      <span className="postal-code">{postCode}</span>
                    </ItemValue>
                  </ListItemLeft>
                  <ListItemRight>
                    <ItemLabel>Country</ItemLabel>
                    <ItemValue paddingValue="10">
                      <span className="country-name">{country}</span>
                    </ItemValue>
                  </ListItemRight>
                </WrapIntoSingle>
              </List>
            </Card>
          </CardContainer>
        </VCard>
        <br/>
      </div>
    );
  }
}
