import React, { Component } from "react";
import TextField from "../TextField";
import {
  FormContainer,
  RowLine,
  ButtonContainer,
  Button,
  FileUploadContainer,
  UploadLabel,
  FileUploadInput,
  FormHeading,
  SectionHeading
} from "./DetailsForm.styled";

export default class DetailsForm extends Component {
  render() {
    return (
      <FormContainer>
        <FormHeading>hCard Builder</FormHeading>
        <SectionHeading>Personal Details</SectionHeading>
        <RowLine>
          <TextField
            id={"given-name"}
            type={"text"}
            label={"Given Name"}
            maxLength={255}
            placeholder={""}
            name={"givenName"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />
          <TextField
            id={"sur-name"}
            type={"text"}
            label={"Surame"}
            maxLength={255}
            placeholder={""}
            name={"surName"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />
        </RowLine>
        <RowLine>
          <TextField
            id={"email"}
            type={"email"}
            label={"Email"}
            maxLength={255}
            placeholder={""}
            name={"email"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />
          <TextField
            id={"phone"}
            type={"number"}
            label={"Phone"}
            maxLength={10}
            placeholder={""}
            name={"phone"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />
        </RowLine>

        <SectionHeading>Address Details</SectionHeading>
        <RowLine>
          <TextField
            id={"house-name"}
            type={"text"}
            label={"House Name or #"}
            maxLength={255}
            placeholder={""}
            name={"houseName"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />

          <TextField
            id={"street"}
            type={"text"}
            label={"Street"}
            maxLength={255}
            placeholder={""}
            name={"street"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />
        </RowLine>
        <RowLine>
          <TextField
            id={"suburb"}
            type={"text"}
            label={"Suburb"}
            maxLength={255}
            placeholder={""}
            name={"suburb"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />

          <TextField
            id={"state"}
            type={"text"}
            label={"State"}
            maxLength={255}
            placeholder={""}
            name={"state"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />
        </RowLine>
        <RowLine>
          <TextField
            id={"post-code"}
            type={"number"}
            label={"Post Code"}
            maxLength={4}
            placeholder={""}
            name={"postCode"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />

          <TextField
            id={"country"}
            type={"text"}
            label={"Country"}
            maxLength={255}
            placeholder={""}
            name={"country"}
            onChange={e => this.props.handleFormChange(e)}
            autoComplete={"off"}
          />
        </RowLine>
        <RowLine>
          <FileUploadContainer>
            <UploadLabel>Upload Avatar</UploadLabel>
            <FileUploadInput
              name={"avatar"}
              type="file"
              id="image-avatar"
              accept="image/*"
              onClick={e => console.log("iamge")}
              onChange={e => this.props.handleAvatarUpload(e)}
            />
          </FileUploadContainer>
          <ButtonContainer>
            <Button
              backgroundColor="#3498db"
              color="white"
              onClick={e => console.log("create hcard")}
            >
              Create hCard
            </Button>
          </ButtonContainer>
        </RowLine>
      </FormContainer>
    );
  }
}
