import styled from "styled-components";

const CardContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  font-family: sans-serif;
`;
const Card = styled.section`
  background: #ffffff;
  padding: 0;
  width: 100%;
  box-shadow: 2px 2px 5px 0px #546271;
`;
const List = styled.div`
  list-style: none;
  padding: 0;
`;
const ListItem = styled.div`
  height: 20px;
  margin: 20px;
  display: flex;
  border-bottom: 1px solid #546271;
`;
const Header = styled.header`
  background: #2c3e50;
  height: 100px;
  display: flex;
`;
const ImageCover = styled.div`
  width: 80px;
  height: 80px;
  margin-left: auto;
  margin-right: 10px;
  -webkit-transform: translateY(-50%);
  -ms-transform: translateY(-50%);
  transform: translateY(50%);
  overflow: hidden;
  position: relative;
  z-index: 10;
`;
const AvatarImage = styled.img`
  display: block;
  width: 100%;
  height: 100%;
  object-fit: cover;
`;
const CardTitle = styled.span`
  align-self: flex-end;
  font-size: 20px;
  color: #fff;
  font-family: sans-serif;
  padding: 10px 20px;
`;
const VCard = styled.div`
  min-width: 300px;
  max-width: 350px;
  flex-basis: auto;
  flex-grow: 1;
`;
const ItemLabel = styled.span`
  font-size: 10px;
  text-transform: uppercase;
  flex: 1;
  color: #546271;
`;
const ItemValue = styled.span`
  font-size: 12px;
  flex: 3;
  padding: ${props => `0 ${props.paddingValue}px`};
`;
const DodgyAddressLine = styled.div`
  margin-top: 25px;
`;
const WrapIntoSingle = styled.div`
  display: flex;
`;
const ListItemLeft = styled(ListItem)`
  width: 50%;
  margin-right: 0;
`;
const ListItemRight = styled(ListItem)`
  width: 50%;
  margin-left: 0;
`;
const Link = styled.a`
  color: #000;
  text-decoration: none;
`;
const PlaceholderHeading = styled.label`
  text-transform: uppercase;
  float: right;
  color: #b0b8bc;
  font-family: sans-serif;
`;

export {
  CardContainer,
  Card,
  List,
  ListItem,
  Header,
  ImageCover,
  AvatarImage,
  CardTitle,
  VCard,
  ItemLabel,
  ItemValue,
  DodgyAddressLine,
  WrapIntoSingle,
  ListItemLeft,
  ListItemRight,
  Link,
  PlaceholderHeading
};
